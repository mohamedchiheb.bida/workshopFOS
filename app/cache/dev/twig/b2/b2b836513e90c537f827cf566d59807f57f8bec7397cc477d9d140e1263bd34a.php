<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_78723faa6cddb73a5f27b316cec0eccb106a614a5936697a20bf67f9d06a4b0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>
<head>
<title>Free Adidas Website Template | Home :: w3layouts</title>
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/codepen.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fontawsom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link href='";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800"), "html", null, true);
        echo "' rel='stylesheet' type='text/css'>
<script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
        \$(document).ready(function() {
            \$(\".dropdown img.flag\").addClass(\"flagvisibility\");

            \$(\".dropdown dt a\").click(function() {
                \$(\".dropdown dd ul\").toggle();
            });
                        
            \$(\".dropdown dd ul li a\").click(function() {
                var text = \$(this).html();
                \$(\".dropdown dt a span\").html(text);
                \$(\".dropdown dd ul\").hide();
                \$(\"#result\").html(\"Selected value is: \" + getSelectedValue(\"sample\"));
            });
                        
            function getSelectedValue(id) {
                return \$(\"#\" + id).find(\"dt a span.value\").html();
            }

            \$(document).bind('click', function(e) {
                var \$clicked = \$(e.target);
                if (! \$clicked.parents().hasClass(\"dropdown\"))
                    \$(\".dropdown dd ul\").hide();
            });


            \$(\"#flagSwitcher\").click(function() {
                \$(\".dropdown img.flag\").toggleClass(\"flagvisibility\");
            });
        });
     </script>
<!-- start menu -->     
<link href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "\"></script>
<script>\$(document).ready(function(){\$(\".megamenu\").megamenu();});</script>
<!-- end menu -->
<!-- top scrolling -->
<script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/move-top.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/easing.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\">
\t\tjQuery(document).ready(function(\$) {
\t\t\t\$(\".scroll\").click(function(event){\t\t
\t\t\t\tevent.preventDefault();
\t\t\t\t\$('html,body').animate({scrollTop:\$(this.hash).offset().top},1200);
\t\t\t});
\t\t});
\t</script>
</head>
<body>
  <div class=\"header-top\">
\t <div class=\"wrap\"> 
\t\t<div class=\"logo\">
\t\t\t<a href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/test"), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" alt=\"\"/></a>
\t    </div>
\t    <div class=\"cssmenu\">
\t\t   <ul>
\t\t\t <li class=\"active\"><a href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("register.html"), "html", null, true);
        echo "\">Sign up & Save</a></li> 
\t\t\t <li><a href=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Store Locator</a></li> 
\t\t\t <li><a href=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("login.html"), "html", null, true);
        echo "\">My Account</a></li> 
\t\t\t <li><a href=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("checkout.html"), "html", null, true);
        echo "\">CheckOut</a></li> 
\t\t   </ul>
\t\t</div>
\t\t<ul class=\"icon2 sub-icon2 profile_img\">
\t\t\t<li><a class=\"active-icon c2\" href=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\"> </a>
\t\t\t\t<ul class=\"sub-icon2 list\">
\t\t\t\t\t<li><h3>Products</h3><a href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(""), "html", null, true);
        echo "\"></a></li>
\t\t\t\t\t<li><p>Lorem ipsum dolor sit amet, consectetuer  <a href=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(""), "html", null, true);
        echo "\">adipiscing elit, sed diam</a></p></li>
\t\t\t\t</ul>
\t\t\t</li>
\t\t</ul>
\t\t<div class=\"clear\"></div>
 \t</div>
   </div>
   <div class=\"header-bottom\">
   \t<div class=\"wrap\">
   \t\t<!-- start header menu -->
\t\t<ul class=\"megamenu skyblue\">
\t\t    <li><a class=\"color1\" href=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Acceuil</a></li>
\t\t\t<li class=\"grid\"><a class=\"color2\">Service</a>
\t\t\t\t<div class=\"megapanel\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>Traitement Service</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/ajouterService"), "html", null, true);
        echo "\">Ajouter</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/addOffre"), "html", null, true);
        echo "\">Demander</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4 class=\"top\">Consulter Service</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/list"), "html", null, true);
        echo "\">Acceuil Service</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/MesService"), "html", null, true);
        echo "\">Mes Services </a></li>
                                                                        <li><a href=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/listOffre"), "html", null, true);
        echo "\">Acceuil Offre</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/MesDemande"), "html", null, true);
        echo "\">Mes Demandes </a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>style zone</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<img src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/serv.jpg"), "html", null, true);
        echo "\" alt=\"\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</li>
  \t\t\t   <li class=\"active grid\"><a class=\"color4\" href=\"#\">Women</a>
\t\t\t\t<div class=\"megapanel\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>shop</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>help</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>my company</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>account</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">login</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create an account</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">my shopping bag</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>popular</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t <div class=\"h_nav\">
\t\t\t\t\t\t   <img src=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/nav_img1.jpg"), "html", null, true);
        echo "\" alt=\"\"/>
\t\t\t\t\t\t </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col2\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t</div>
    \t\t\t</li>\t\t\t\t
\t\t\t\t<li><a class=\"color5\" href=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Kids</a>
\t\t\t\t<div class=\"megapanel\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>popular</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">login</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4 class=\"top\">man</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 237
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>style zone</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 247
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<img src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/nav_img2.jpg"), "html", null, true);
        echo "\" alt=\"\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</li>
\t\t\t\t<li><a class=\"color6\" href=\"";
        // line 262
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Sale</a>
\t\t\t\t<div class=\"megapanel\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>shop</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 272
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 273
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4 class=\"top\">my company</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 280
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 281
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 282
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 283
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 284
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 285
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>man</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 293
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 296
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 297
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 298
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>help</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 306
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 307
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 308
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 309
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 311
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>account</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 319
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">login</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 320
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create an account</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 321
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 322
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">my shopping bag</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 323
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 324
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>my company</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 332
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 333
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 334
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 335
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 336
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 337
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>popular</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 345
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 346
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 347
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 348
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 349
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 350
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col2\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</li>
\t\t\t\t<li><a class=\"color7\" href=\"";
        // line 364
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Customize</a>
\t\t\t\t<div class=\"megapanel\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>shop</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 371
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 372
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 373
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 374
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 375
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 376
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>help</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 384
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 385
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 386
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 387
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 388
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 389
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>my company</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 397
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 398
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 399
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 400
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 401
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 402
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>account</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">login</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 411
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create an account</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 412
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 413
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">my shopping bag</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 414
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 415
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>my company</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 425
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 426
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 427
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 428
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>popular</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 436
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 437
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 438
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 439
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 440
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 441
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col2\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t</div>
    \t\t\t\t</div>
\t\t\t\t</li>
\t\t\t\t<li><a class=\"color8\" href=\"";
        // line 455
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Shop</a>
\t\t\t\t<div class=\"megapanel\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>style zone</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 463
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 464
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 465
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 466
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>popular</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 474
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 475
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 476
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 477
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">login</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4 class=\"top\">man</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 484
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 485
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 486
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 487
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</li>
\t\t\t\t<li><a class=\"color9\" href=\"";
        // line 497
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Football</a>
\t\t\t\t<div class=\"megapanel\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>shop</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 504
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 505
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 506
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 507
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 508
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 509
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>help</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 517
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 518
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 519
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 520
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 521
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 522
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>my company</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 530
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 531
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 532
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 533
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 534
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 535
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>account</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 543
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">login</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 544
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create an account</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 545
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 546
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">my shopping bag</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 547
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">brands</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 548
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">create wishlist</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>my company</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 556
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">trends</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 557
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">sale</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 558
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 559
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 560
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 561
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col1\">
\t\t\t\t\t\t\t<div class=\"h_nav\">
\t\t\t\t\t\t\t\t<h4>popular</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 569
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 570
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">men</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 571
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">women</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 572
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 573
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">kids</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 574
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">style videos</a></li>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col2\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t\t<div class=\"col1\"></div>
\t\t\t\t\t</div>
    \t\t\t\t</div>
\t\t\t\t</li>
\t\t\t\t<li><a class=\"color10\" href=\"";
        // line 588
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Running</a></li>
\t\t\t\t<li><a class=\"color11\" href=\"";
        // line 589
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Originals</a></li>
\t\t\t\t<li><a class=\"color12\" href=\"";
        // line 590
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Basketball</a></li>
\t\t   </ul>
\t\t   <div class=\"clear\"></div>
     \t</div>
       </div>
        <div>
            ";
        // line 596
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 598
        echo "        </div>

        <div class=\"footer\">
       \t  <div class=\"footer-top\">
       \t\t<div class=\"wrap\">
       \t\t\t   <div class=\"col_1_of_footer-top span_1_of_footer-top\">
\t\t\t\t  \t <ul class=\"f_list\">
\t\t\t\t  \t \t<li><img src=\"";
        // line 605
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/f_icon.png"), "html", null, true);
        echo "\" alt=\"\"/><span class=\"delivery\">Free delivery on all orders over �100*</span></li>
\t\t\t\t  \t </ul>
\t\t\t\t   </div>
\t\t\t\t   <div class=\"col_1_of_footer-top span_1_of_footer-top\">
\t\t\t\t  \t<ul class=\"f_list\">
\t\t\t\t  \t \t<li><img src=\"";
        // line 610
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/f_icon1.png"), "html", null, true);
        echo "\" alt=\"\"/><span class=\"delivery\">Customer Service :<span class=\"orange\"> (800) 000-2587 (freephone)</span></span></li>
\t\t\t\t  \t </ul>
\t\t\t\t   </div>
\t\t\t\t   <div class=\"col_1_of_footer-top span_1_of_footer-top\">
\t\t\t\t  \t<ul class=\"f_list\">
\t\t\t\t  \t \t<li><img src=\"";
        // line 615
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/f_icon2.png"), "html", null, true);
        echo "\" alt=\"\"/><span class=\"delivery\">Fast delivery & free returns</span></li>
\t\t\t\t  \t </ul>
\t\t\t\t   </div>
\t\t\t\t  <div class=\"clear\"></div>
\t\t\t </div>
       \t </div>
       \t <div class=\"footer-middle\">
       \t \t<div class=\"wrap\">
       \t \t\t<div class=\"section group\">
\t\t\t\t<div class=\"col_1_of_middle span_1_of_middle\">
\t\t\t\t\t<dl id=\"sample\" class=\"dropdown\">
\t\t\t        <dt><a href=\"";
        // line 626
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\"><span>Please Select a Country</span></a></dt>
\t\t\t        <dd>
\t\t\t            <ul>
\t\t\t                <li><a href=\"";
        // line 629
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Australia<img class=\"flag\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/as.png"), "html", null, true);
        echo "\" alt=\"\" /><span class=\"value\">AS</span></a></li>
\t\t\t                <li><a href=\"";
        // line 630
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Sri Lanka<img class=\"flag\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/srl.png"), "html", null, true);
        echo "\" alt=\"\" /><span class=\"value\">SL</span></a></li>
\t\t\t                <li><a href=\"";
        // line 631
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Newziland<img class=\"flag\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/nz.png"), "html", null, true);
        echo "\" alt=\"\" /><span class=\"value\">NZ</span></a></li>
\t\t\t                <li><a href=\"";
        // line 632
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">Pakistan<img class=\"flag\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/pk.png"), "html", null, true);
        echo "\" alt=\"\" /><span class=\"value\">Pk</span></a></li>
\t\t\t                <li><a href=\"";
        // line 633
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">United Kingdom<img class=\"flag\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/uk.png"), "html", null, true);
        echo "\" alt=\"\" /><span class=\"value\">UK</span></a></li>
\t\t\t                <li><a href=\"";
        // line 634
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\">United States<img class=\"flag\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/us.png"), "html", null, true);
        echo "\" alt=\"\" /><span class=\"value\">US</span></a></li>
\t\t\t            </ul>
\t\t\t         </dd>
   \t\t\t\t    </dl>
   \t\t\t\t </div>
\t\t\t\t<div class=\"col_1_of_middle span_1_of_middle\">
\t\t\t\t\t<ul class=\"f_list1\">
\t\t\t\t\t\t<li><span class=\"m_8\">Sign up for email and Get 15% off</span>
\t\t\t\t\t\t<div class=\"search\">
                                                    <form method=\"post\">
\t\t\t\t\t\t\t<input type=\"text\" name=\"search\" class=\"textbox\" value=\"Search\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Search';}\">
\t\t\t\t\t\t\t<input type=\"submit\" value=\"Subscribe\" id=\"submit\" name=\"chercher\">
                                                    </form>
\t\t\t\t\t\t\t<div id=\"response\"> </div>
\t\t\t \t\t\t</div><div class=\"clear\"></div>
\t\t\t \t\t    </li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"clear\"></div>
\t\t\t   </div>
       \t \t</div>
       \t </div>
       \t <div class=\"footer-bottom\">
       \t \t<div class=\"wrap\">
       \t \t\t<div class=\"section group\">
\t\t\t\t<div class=\"col_1_of_5 span_1_of_5\">
\t\t\t\t\t<h3 class=\"m_9\">Shop</h3>
\t\t\t\t\t<ul class=\"sub_list\">
\t\t\t\t\t\t<h4 class=\"m_10\">Men</h4>
\t\t\t\t\t    <li><a href=\"";
        // line 663
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop."), "html", null, true);
        echo "\">Men's Shoes</a></li>
\t\t\t            <li><a href=\"";
        // line 664
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Men's Clothing</a></li>
\t\t\t            <li><a href=\"";
        // line 665
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Men's Accessories</a></li>
\t\t\t        </ul>
\t\t\t             <ul class=\"sub_list\">
\t\t\t\t            <h4 class=\"m_10\">Women</h4>
\t\t\t\t            <li><a href=\"";
        // line 669
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Women's Shoes</a></li>
\t\t\t\t            <li><a href=\"";
        // line 670
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Women's Clothing</a></li>
\t\t\t\t            <li><a href=\"";
        // line 671
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Women's Accessories</a></li>
\t\t\t\t         </ul>
\t\t\t\t         <ul class=\"sub_list\">
\t\t\t\t            <h4 class=\"m_10\">Kids</h4>
\t\t\t\t            <li><a href=\"";
        // line 675
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Kids Shoes</a></li>
\t\t\t\t            <li><a href=\"";
        // line 676
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Kids Clothing</a></li>
\t\t\t\t            <li><a href=\"";
        // line 677
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Kids Accessories</a></li>
\t\t\t\t         </ul>
\t\t\t\t        <ul class=\"sub_list\">
\t\t\t\t            <h4 class=\"m_10\">style</h4>
\t\t\t\t            <li><a href=\"";
        // line 681
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Porsche Design Sport</a></li>
\t\t\t\t            <li><a href=\"";
        // line 682
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Porsche Design Shoes</a></li>
\t\t\t\t            <li><a href=\"";
        // line 683
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Porsche Design Clothing</a></li>
\t\t\t\t        </ul>
\t\t\t\t        <ul class=\"sub_list\">
\t\t\t\t            <h4 class=\"m_10\">Adidas Neo Label</h4>
\t\t\t\t            <li><a href=\"";
        // line 687
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Adidas NEO Shoes</a></li>
\t\t\t\t            <li><a href=\"";
        // line 688
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Adidas NEO Clothing</a></li>
\t\t\t\t        </ul>
\t\t\t\t        <ul class=\"sub_list1\">
\t\t\t\t            <h4 class=\"m_10\">Customise</h4>
\t\t\t\t            <li><a href=\"";
        // line 692
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">mi adidas</a></li>
\t\t\t\t            <li><a href=\"";
        // line 693
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">mi team</a></li>
\t\t\t\t            <li><a href=\"";
        // line 694
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">new arrivals</a></li>
\t\t\t\t        </ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"col_1_of_5 span_1_of_5\">
\t\t\t\t\t<h3 class=\"m_9\">Sports</h3>
\t\t\t\t\t<ul class=\"list1\">
\t\t\t\t\t    <li><a href=\"";
        // line 700
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Basketball</a></li>
\t\t\t            <li><a href=\"shop.html\">Football</a></li>
\t\t\t            <li><a href=\"shop.html\">Football Boots</a></li>
\t\t\t            <li><a href=\"shop.html\">Predator</a></li>
\t\t\t            <li><a href=\"shop.html\">F50</a></li>
\t\t\t            <li><a href=\"shop.html\">Football Clothing</a></li>
\t\t\t            <li><a href=\"shop.html\">Golf</a></li>
\t\t\t            <li><a href=\"shop.html\">Golf Shoes</a></li>
\t\t\t            <li><a href=\"shop.html\">Golf Clothing</a></li>
\t\t\t            <li><a href=\"shop.html\">Outdoor</a></li>
\t\t\t            <li><a href=\"shop.html\">Outdoor Shoes</a></li>
\t\t\t            <li><a href=\"shop.html\">Outdoor Clothing</a></li>
\t\t\t            <li><a href=\"shop.html\">Rugby</a></li>
\t\t\t            <li><a href=\"shop.html\">Running</a></li>
\t\t\t            <li><a href=\"shop.html\">Running Shoes</a></li>
\t\t\t            <li><a href=\"shop.html\">Boost</a></li>
\t\t\t            <li><a href=\"shop.html\">Supernova</a></li>
\t\t\t            <li><a href=\"shop.html\">Running Clothing</a></li>
\t\t\t            <li><a href=\"shop.html\">Swimming</a></li>
\t\t\t            <li><a href=\"shop.html\">Tennis</a></li>
\t\t\t            <li><a href=\"shop.html\">Tennis Shoes</a></li>
\t\t\t            <li><a href=\"shop.html\">Tennis Clothing</a></li>
\t\t\t            <li><a href=\"shop.html\">Training</a></li>
\t\t\t            <li><a href=\"shop.html\">Training Shoes</a></li>
\t\t\t            <li><a href=\"shop.html\">Training Clothing</a></li>
\t\t\t            <li><a href=\"shop.html\">Training Accessories</a></li>
\t\t\t            <li><a href=\"shop.html\">miCoach</a></li>
\t\t\t            <li><a href=\"shop.html\">All Sports</a></li>
\t\t\t         </ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"col_1_of_5 span_1_of_5\">
\t\t\t\t\t<h3 class=\"m_9\">Originals</h3>
\t\t\t\t\t<ul class=\"list1\">
\t\t\t\t\t    <li><a href=\"shop.html\">Originals Shoes</a></li>
\t\t\t            <li><a href=\"shop.html\">Gazelle</a></li>
\t\t\t            <li><a href=\"shop.html\">Samba</a></li>
\t\t\t            <li><a href=\"shop.html\">LA Trainer</a></li>
\t\t\t            <li><a href=\"shop.html\">Superstar</a></li>
\t\t\t            <li><a href=\"shop.html\">SL</a></li>
\t\t\t            <li><a href=\"shop.html\">ZX</a></li>
\t\t\t            <li><a href=\"shop.html\">Campus</a></li>
\t\t\t            <li><a href=\"shop.html\">Spezial</a></li>
\t\t\t            <li><a href=\"shop.html\">Dragon</a></li>
\t\t\t            <li><a href=\"shop.html\">Originals Clothing</a></li>
\t\t\t            <li><a href=\"shop.html\">Firebird</a></li>
\t\t\t            <li><a href=\"shop.html\">Originals Accessories</a></li>
\t\t\t            <li><a href=\"shop.html\">Men's Originals</a></li>
\t\t\t            <li><a href=\"shop.html\">Women's Originals</a></li>
\t\t\t            <li><a href=\"shop.html\">Kid's Originals</a></li>
\t\t\t            <li><a href=\"shop.html\">All Originals</a></li>
\t\t            </ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"col_1_of_5 span_1_of_5\">
\t\t\t\t\t<h3 class=\"m_9\">Product Types</h3>
\t\t\t\t\t<ul class=\"list1\">
\t\t\t\t\t    <li><a href=\"shop.html\">Shirts</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Pants & Tights</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Shirts</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Jerseys</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Hoodies & Track Tops</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Bags</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Jackets</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Hi Tops</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">SweatShirts</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Socks</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Swimwear</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Tracksuits</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Hats</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Football Boots</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Other Accessories</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Sandals & Flip Flops</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Skirts & Dresseses</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Balls</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Watches</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Fitness Equipment</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Eyewear</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Gloves</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Sports Bras</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Scarves</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Shinguards</a></li>
\t\t\t\t\t    <li><a href=\"shop.html\">Underwear</a></li>
\t\t            </ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"col_1_of_5 span_1_of_5\">
\t\t\t\t\t<h3 class=\"m_9\">Support</h3>
\t\t\t\t\t<ul class=\"list1\">
\t\t\t\t\t   <li><a href=\"shop.html\">Store finder</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Customer Service</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">FAQ</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Online Shop Contact Us</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">about adidas Products</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Size Charts </a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Ordering </a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Payment </a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Shipping </a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Returning</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Using out Site</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Delivery Terms</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Site Map</a></li>
\t\t\t\t\t   <li><a href=\"shop.html\">Gift Card</a></li>
\t\t\t\t\t  
\t\t            </ul>
\t\t            <ul class=\"sub_list2\">
\t\t               <h4 class=\"m_10\">Company Info</h4>
\t\t\t           <li><a href=\"";
        // line 804
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">About Us</a></li>
\t\t\t           <li><a href=\"";
        // line 805
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Careers</a></li>
\t\t\t           <li><a href=\"";
        // line 806
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("shop.html"), "html", null, true);
        echo "\">Press</a></li>
\t\t\t        </ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"clear\"></div>
\t\t\t</div>
       \t \t</div>
       \t </div>
       \t <div class=\"copy\">
       \t   <div class=\"wrap\">
       \t   \t  <p>� All rights reserved | Template by&nbsp;<a href=\"";
        // line 815
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("http://w3layouts.com/"), "html", null, true);
        echo "\"> W3Layouts</a></p>
       \t   </div>
       \t </div>
       </div>
       <script type=\"text/javascript\">
\t\t\t\$(document).ready(function() {
\t\t\t
\t\t\t\tvar defaults = {
\t\t  \t\t\tcontainerID: 'toTop', // fading element id
\t\t\t\t\tcontainerHoverID: 'toTopHover', // fading element hover id
\t\t\t\t\tscrollSpeed: 1200,
\t\t\t\t\teasingType: 'linear' 
\t\t \t\t};
\t\t\t\t
\t\t\t\t
\t\t\t\t\$().UItoTop({ easingType: 'easeOutQuart' });
\t\t\t\t
\t\t\t});
\t\t</script>
        <a href=\"";
        // line 834
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\" id=\"toTop\" style=\"display: block;\"><span id=\"toTopHover\" style=\"opacity: 1;\"></span></a>
</body>
</html>
";
    }

    // line 596
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 597
        echo "            ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1623 => 597,  1620 => 596,  1612 => 834,  1590 => 815,  1578 => 806,  1574 => 805,  1570 => 804,  1463 => 700,  1454 => 694,  1450 => 693,  1446 => 692,  1439 => 688,  1435 => 687,  1428 => 683,  1424 => 682,  1420 => 681,  1413 => 677,  1409 => 676,  1405 => 675,  1398 => 671,  1394 => 670,  1390 => 669,  1383 => 665,  1379 => 664,  1375 => 663,  1341 => 634,  1335 => 633,  1329 => 632,  1323 => 631,  1317 => 630,  1311 => 629,  1305 => 626,  1291 => 615,  1283 => 610,  1275 => 605,  1266 => 598,  1264 => 596,  1255 => 590,  1251 => 589,  1247 => 588,  1230 => 574,  1226 => 573,  1222 => 572,  1218 => 571,  1214 => 570,  1210 => 569,  1199 => 561,  1195 => 560,  1191 => 559,  1187 => 558,  1183 => 557,  1179 => 556,  1168 => 548,  1164 => 547,  1160 => 546,  1156 => 545,  1152 => 544,  1148 => 543,  1137 => 535,  1133 => 534,  1129 => 533,  1125 => 532,  1121 => 531,  1117 => 530,  1106 => 522,  1102 => 521,  1098 => 520,  1094 => 519,  1090 => 518,  1086 => 517,  1075 => 509,  1071 => 508,  1067 => 507,  1063 => 506,  1059 => 505,  1055 => 504,  1045 => 497,  1032 => 487,  1028 => 486,  1024 => 485,  1020 => 484,  1011 => 478,  1007 => 477,  1003 => 476,  999 => 475,  995 => 474,  984 => 466,  980 => 465,  976 => 464,  972 => 463,  968 => 462,  958 => 455,  941 => 441,  937 => 440,  933 => 439,  929 => 438,  925 => 437,  921 => 436,  910 => 428,  906 => 427,  902 => 426,  898 => 425,  894 => 424,  890 => 423,  879 => 415,  875 => 414,  871 => 413,  867 => 412,  863 => 411,  859 => 410,  848 => 402,  844 => 401,  840 => 400,  836 => 399,  832 => 398,  828 => 397,  817 => 389,  813 => 388,  809 => 387,  805 => 386,  801 => 385,  797 => 384,  786 => 376,  782 => 375,  778 => 374,  774 => 373,  770 => 372,  766 => 371,  756 => 364,  739 => 350,  735 => 349,  731 => 348,  727 => 347,  723 => 346,  719 => 345,  708 => 337,  704 => 336,  700 => 335,  696 => 334,  692 => 333,  688 => 332,  677 => 324,  673 => 323,  669 => 322,  665 => 321,  661 => 320,  657 => 319,  646 => 311,  642 => 310,  638 => 309,  634 => 308,  630 => 307,  626 => 306,  615 => 298,  611 => 297,  607 => 296,  603 => 295,  599 => 294,  595 => 293,  584 => 285,  580 => 284,  576 => 283,  572 => 282,  568 => 281,  564 => 280,  555 => 274,  551 => 273,  547 => 272,  543 => 271,  539 => 270,  535 => 269,  525 => 262,  518 => 258,  507 => 250,  503 => 249,  499 => 248,  495 => 247,  491 => 246,  480 => 238,  476 => 237,  472 => 236,  468 => 235,  464 => 234,  460 => 233,  451 => 227,  447 => 226,  443 => 225,  439 => 224,  435 => 223,  431 => 222,  421 => 215,  405 => 202,  396 => 196,  392 => 195,  388 => 194,  384 => 193,  380 => 192,  376 => 191,  365 => 183,  361 => 182,  357 => 181,  353 => 180,  349 => 179,  345 => 178,  334 => 170,  330 => 169,  326 => 168,  322 => 167,  318 => 166,  314 => 165,  303 => 157,  299 => 156,  295 => 155,  291 => 154,  287 => 153,  283 => 152,  272 => 144,  268 => 143,  264 => 142,  260 => 141,  256 => 140,  252 => 139,  238 => 128,  227 => 120,  223 => 119,  219 => 118,  215 => 117,  211 => 116,  200 => 108,  196 => 107,  192 => 106,  188 => 105,  178 => 98,  174 => 97,  163 => 89,  149 => 78,  145 => 77,  140 => 75,  133 => 71,  129 => 70,  125 => 69,  121 => 68,  112 => 64,  95 => 50,  91 => 49,  84 => 45,  80 => 44,  44 => 11,  40 => 10,  36 => 9,  32 => 8,  28 => 7,  20 => 1,);
    }
}
/* <!DOCTYPE HTML>*/
/* <html>*/
/* <head>*/
/* <title>Free Adidas Website Template | Home :: w3layouts</title>*/
/* <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/* <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/* <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all" />*/
/* <link href="{{asset('css/codepen.css')}}" rel="stylesheet" type="text/css" media="all" />*/
/* <link href="{{asset('css/fontawsom.css')}}" rel="stylesheet" type="text/css" media="all" />*/
/* <link href='{{asset('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800')}}' rel='stylesheet' type='text/css'>*/
/* <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>*/
/* <script type="text/javascript">*/
/*         $(document).ready(function() {*/
/*             $(".dropdown img.flag").addClass("flagvisibility");*/
/* */
/*             $(".dropdown dt a").click(function() {*/
/*                 $(".dropdown dd ul").toggle();*/
/*             });*/
/*                         */
/*             $(".dropdown dd ul li a").click(function() {*/
/*                 var text = $(this).html();*/
/*                 $(".dropdown dt a span").html(text);*/
/*                 $(".dropdown dd ul").hide();*/
/*                 $("#result").html("Selected value is: " + getSelectedValue("sample"));*/
/*             });*/
/*                         */
/*             function getSelectedValue(id) {*/
/*                 return $("#" + id).find("dt a span.value").html();*/
/*             }*/
/* */
/*             $(document).bind('click', function(e) {*/
/*                 var $clicked = $(e.target);*/
/*                 if (! $clicked.parents().hasClass("dropdown"))*/
/*                     $(".dropdown dd ul").hide();*/
/*             });*/
/* */
/* */
/*             $("#flagSwitcher").click(function() {*/
/*                 $(".dropdown img.flag").toggleClass("flagvisibility");*/
/*             });*/
/*         });*/
/*      </script>*/
/* <!-- start menu -->     */
/* <link href="{{asset('css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />*/
/* <script type="text/javascript" src="{{asset('js/megamenu.js')}}"></script>*/
/* <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>*/
/* <!-- end menu -->*/
/* <!-- top scrolling -->*/
/* <script type="text/javascript" src="{{asset('js/move-top.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('js/easing.js')}}"></script>*/
/*    <script type="text/javascript">*/
/* 		jQuery(document).ready(function($) {*/
/* 			$(".scroll").click(function(event){		*/
/* 				event.preventDefault();*/
/* 				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);*/
/* 			});*/
/* 		});*/
/* 	</script>*/
/* </head>*/
/* <body>*/
/*   <div class="header-top">*/
/* 	 <div class="wrap"> */
/* 		<div class="logo">*/
/* 			<a href="{{asset('app_dev.php/test')}}"><img src="{{asset('images/logo.png')}}" alt=""/></a>*/
/* 	    </div>*/
/* 	    <div class="cssmenu">*/
/* 		   <ul>*/
/* 			 <li class="active"><a href="{{asset('register.html')}}">Sign up & Save</a></li> */
/* 			 <li><a href="{{asset('shop.html')}}">Store Locator</a></li> */
/* 			 <li><a href="{{asset('login.html')}}">My Account</a></li> */
/* 			 <li><a href="{{asset('checkout.html')}}">CheckOut</a></li> */
/* 		   </ul>*/
/* 		</div>*/
/* 		<ul class="icon2 sub-icon2 profile_img">*/
/* 			<li><a class="active-icon c2" href="{{asset('#')}}"> </a>*/
/* 				<ul class="sub-icon2 list">*/
/* 					<li><h3>Products</h3><a href="{{asset('')}}"></a></li>*/
/* 					<li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="{{asset('')}}">adipiscing elit, sed diam</a></p></li>*/
/* 				</ul>*/
/* 			</li>*/
/* 		</ul>*/
/* 		<div class="clear"></div>*/
/*  	</div>*/
/*    </div>*/
/*    <div class="header-bottom">*/
/*    	<div class="wrap">*/
/*    		<!-- start header menu -->*/
/* 		<ul class="megamenu skyblue">*/
/* 		    <li><a class="color1" href="{{asset('#')}}">Acceuil</a></li>*/
/* 			<li class="grid"><a class="color2">Service</a>*/
/* 				<div class="megapanel">*/
/* 					<div class="row">*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>Traitement Service</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('app_dev.php/ajouterService')}}">Ajouter</a></li>*/
/* 									<li><a href="{{asset('app_dev.php/addOffre')}}">Demander</a></li>*/
/* 								*/
/* 								</ul>	*/
/* 							</div>*/
/* 							<div class="h_nav">*/
/* 								<h4 class="top">Consulter Service</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('app_dev.php/list')}}">Acceuil Service</a></li>*/
/* 									<li><a href="{{asset('app_dev.php/MesService')}}">Mes Services </a></li>*/
/*                                                                         <li><a href="{{asset('app_dev.php/listOffre')}}">Acceuil Offre</a></li>*/
/* 									<li><a href="{{asset('app_dev.php/MesDemande')}}">Mes Demandes </a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>style zone</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<img src="{{asset('images/serv.jpg')}}" alt=""/>*/
/* 					</div>*/
/* 				</div>*/
/* 				</li>*/
/*   			   <li class="active grid"><a class="color4" href="#">Women</a>*/
/* 				<div class="megapanel">*/
/* 					<div class="row">*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>shop</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>help</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>my company</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>												*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>account</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">login</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create an account</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">my shopping bag</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 								</ul>	*/
/* 							</div>						*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>popular</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 						 <div class="h_nav">*/
/* 						   <img src="{{asset('images/nav_img1.jpg')}}" alt=""/>*/
/* 						 </div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col2"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 					</div>*/
/* 					</div>*/
/*     			</li>				*/
/* 				<li><a class="color5" href="{{asset('#')}}">Kids</a>*/
/* 				<div class="megapanel">*/
/* 					<div class="row">*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>popular</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">login</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 							<div class="h_nav">*/
/* 								<h4 class="top">man</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>style zone</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<img src="{{asset('images/nav_img2.jpg')}}" alt=""/>*/
/* 					</div>*/
/* 				</div>*/
/* 				</li>*/
/* 				<li><a class="color6" href="{{asset('#')}}">Sale</a>*/
/* 				<div class="megapanel">*/
/* 					<div class="row">*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>shop</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 								</ul>	*/
/* 							</div>	*/
/* 							<div class="h_nav">*/
/* 								<h4 class="top">my company</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>												*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>man</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>						*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>help</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>account</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">login</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create an account</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">my shopping bag</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 								</ul>	*/
/* 							</div>						*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>my company</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>popular</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col2"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 					</div>*/
/* 				</div>*/
/* 				</li>*/
/* 				<li><a class="color7" href="{{asset('#')}}">Customize</a>*/
/* 				<div class="megapanel">*/
/* 					<div class="row">*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>shop</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>help</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>my company</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>												*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>account</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">login</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create an account</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">my shopping bag</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 								</ul>	*/
/* 							</div>						*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>my company</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>popular</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col2"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 					</div>*/
/*     				</div>*/
/* 				</li>*/
/* 				<li><a class="color8" href="{{asset('#')}}">Shop</a>*/
/* 				<div class="megapanel">*/
/* 					<div class="row">*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>style zone</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>popular</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">login</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 							<div class="h_nav">*/
/* 								<h4 class="top">man</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 					</div>*/
/* 				</div>*/
/* 				</li>*/
/* 				<li><a class="color9" href="{{asset('#')}}">Football</a>*/
/* 				<div class="megapanel">*/
/* 					<div class="row">*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>shop</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>help</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>							*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>my company</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>												*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>account</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">login</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create an account</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">my shopping bag</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">brands</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">create wishlist</a></li>*/
/* 								</ul>	*/
/* 							</div>						*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>my company</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">trends</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">sale</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col1">*/
/* 							<div class="h_nav">*/
/* 								<h4>popular</h4>*/
/* 								<ul>*/
/* 									<li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">men</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">women</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">accessories</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">kids</a></li>*/
/* 									<li><a href="{{asset('shop.html')}}">style videos</a></li>*/
/* 								</ul>	*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col2"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 						<div class="col1"></div>*/
/* 					</div>*/
/*     				</div>*/
/* 				</li>*/
/* 				<li><a class="color10" href="{{asset('#')}}">Running</a></li>*/
/* 				<li><a class="color11" href="{{asset('#')}}">Originals</a></li>*/
/* 				<li><a class="color12" href="{{asset('#')}}">Basketball</a></li>*/
/* 		   </ul>*/
/* 		   <div class="clear"></div>*/
/*      	</div>*/
/*        </div>*/
/*         <div>*/
/*             {% block fos_user_content %}*/
/*             {% endblock fos_user_content %}*/
/*         </div>*/
/* */
/*         <div class="footer">*/
/*        	  <div class="footer-top">*/
/*        		<div class="wrap">*/
/*        			   <div class="col_1_of_footer-top span_1_of_footer-top">*/
/* 				  	 <ul class="f_list">*/
/* 				  	 	<li><img src="{{asset('images/f_icon.png')}}" alt=""/><span class="delivery">Free delivery on all orders over �100*</span></li>*/
/* 				  	 </ul>*/
/* 				   </div>*/
/* 				   <div class="col_1_of_footer-top span_1_of_footer-top">*/
/* 				  	<ul class="f_list">*/
/* 				  	 	<li><img src="{{asset('images/f_icon1.png')}}" alt=""/><span class="delivery">Customer Service :<span class="orange"> (800) 000-2587 (freephone)</span></span></li>*/
/* 				  	 </ul>*/
/* 				   </div>*/
/* 				   <div class="col_1_of_footer-top span_1_of_footer-top">*/
/* 				  	<ul class="f_list">*/
/* 				  	 	<li><img src="{{asset('images/f_icon2.png')}}" alt=""/><span class="delivery">Fast delivery & free returns</span></li>*/
/* 				  	 </ul>*/
/* 				   </div>*/
/* 				  <div class="clear"></div>*/
/* 			 </div>*/
/*        	 </div>*/
/*        	 <div class="footer-middle">*/
/*        	 	<div class="wrap">*/
/*        	 		<div class="section group">*/
/* 				<div class="col_1_of_middle span_1_of_middle">*/
/* 					<dl id="sample" class="dropdown">*/
/* 			        <dt><a href="{{asset('#')}}"><span>Please Select a Country</span></a></dt>*/
/* 			        <dd>*/
/* 			            <ul>*/
/* 			                <li><a href="{{asset('#')}}">Australia<img class="flag" src="{{asset('images/as.png')}}" alt="" /><span class="value">AS</span></a></li>*/
/* 			                <li><a href="{{asset('#')}}">Sri Lanka<img class="flag" src="{{asset('images/srl.png')}}" alt="" /><span class="value">SL</span></a></li>*/
/* 			                <li><a href="{{asset('#')}}">Newziland<img class="flag" src="{{asset('images/nz.png')}}" alt="" /><span class="value">NZ</span></a></li>*/
/* 			                <li><a href="{{asset('#')}}">Pakistan<img class="flag" src="{{asset('images/pk.png')}}" alt="" /><span class="value">Pk</span></a></li>*/
/* 			                <li><a href="{{asset('#')}}">United Kingdom<img class="flag" src="{{asset('images/uk.png')}}" alt="" /><span class="value">UK</span></a></li>*/
/* 			                <li><a href="{{asset('#')}}">United States<img class="flag" src="{{asset('images/us.png')}}" alt="" /><span class="value">US</span></a></li>*/
/* 			            </ul>*/
/* 			         </dd>*/
/*    				    </dl>*/
/*    				 </div>*/
/* 				<div class="col_1_of_middle span_1_of_middle">*/
/* 					<ul class="f_list1">*/
/* 						<li><span class="m_8">Sign up for email and Get 15% off</span>*/
/* 						<div class="search">*/
/*                                                     <form method="post">*/
/* 							<input type="text" name="search" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">*/
/* 							<input type="submit" value="Subscribe" id="submit" name="chercher">*/
/*                                                     </form>*/
/* 							<div id="response"> </div>*/
/* 			 			</div><div class="clear"></div>*/
/* 			 		    </li>*/
/* 					</ul>*/
/* 				</div>*/
/* 				<div class="clear"></div>*/
/* 			   </div>*/
/*        	 	</div>*/
/*        	 </div>*/
/*        	 <div class="footer-bottom">*/
/*        	 	<div class="wrap">*/
/*        	 		<div class="section group">*/
/* 				<div class="col_1_of_5 span_1_of_5">*/
/* 					<h3 class="m_9">Shop</h3>*/
/* 					<ul class="sub_list">*/
/* 						<h4 class="m_10">Men</h4>*/
/* 					    <li><a href="{{asset('shop.')}}">Men's Shoes</a></li>*/
/* 			            <li><a href="{{asset('shop.html')}}">Men's Clothing</a></li>*/
/* 			            <li><a href="{{asset('shop.html')}}">Men's Accessories</a></li>*/
/* 			        </ul>*/
/* 			             <ul class="sub_list">*/
/* 				            <h4 class="m_10">Women</h4>*/
/* 				            <li><a href="{{asset('shop.html')}}">Women's Shoes</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">Women's Clothing</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">Women's Accessories</a></li>*/
/* 				         </ul>*/
/* 				         <ul class="sub_list">*/
/* 				            <h4 class="m_10">Kids</h4>*/
/* 				            <li><a href="{{asset('shop.html')}}">Kids Shoes</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">Kids Clothing</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">Kids Accessories</a></li>*/
/* 				         </ul>*/
/* 				        <ul class="sub_list">*/
/* 				            <h4 class="m_10">style</h4>*/
/* 				            <li><a href="{{asset('shop.html')}}">Porsche Design Sport</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">Porsche Design Shoes</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">Porsche Design Clothing</a></li>*/
/* 				        </ul>*/
/* 				        <ul class="sub_list">*/
/* 				            <h4 class="m_10">Adidas Neo Label</h4>*/
/* 				            <li><a href="{{asset('shop.html')}}">Adidas NEO Shoes</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">Adidas NEO Clothing</a></li>*/
/* 				        </ul>*/
/* 				        <ul class="sub_list1">*/
/* 				            <h4 class="m_10">Customise</h4>*/
/* 				            <li><a href="{{asset('shop.html')}}">mi adidas</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">mi team</a></li>*/
/* 				            <li><a href="{{asset('shop.html')}}">new arrivals</a></li>*/
/* 				        </ul>*/
/* 				</div>*/
/* 				<div class="col_1_of_5 span_1_of_5">*/
/* 					<h3 class="m_9">Sports</h3>*/
/* 					<ul class="list1">*/
/* 					    <li><a href="{{asset('shop.html')}}">Basketball</a></li>*/
/* 			            <li><a href="shop.html">Football</a></li>*/
/* 			            <li><a href="shop.html">Football Boots</a></li>*/
/* 			            <li><a href="shop.html">Predator</a></li>*/
/* 			            <li><a href="shop.html">F50</a></li>*/
/* 			            <li><a href="shop.html">Football Clothing</a></li>*/
/* 			            <li><a href="shop.html">Golf</a></li>*/
/* 			            <li><a href="shop.html">Golf Shoes</a></li>*/
/* 			            <li><a href="shop.html">Golf Clothing</a></li>*/
/* 			            <li><a href="shop.html">Outdoor</a></li>*/
/* 			            <li><a href="shop.html">Outdoor Shoes</a></li>*/
/* 			            <li><a href="shop.html">Outdoor Clothing</a></li>*/
/* 			            <li><a href="shop.html">Rugby</a></li>*/
/* 			            <li><a href="shop.html">Running</a></li>*/
/* 			            <li><a href="shop.html">Running Shoes</a></li>*/
/* 			            <li><a href="shop.html">Boost</a></li>*/
/* 			            <li><a href="shop.html">Supernova</a></li>*/
/* 			            <li><a href="shop.html">Running Clothing</a></li>*/
/* 			            <li><a href="shop.html">Swimming</a></li>*/
/* 			            <li><a href="shop.html">Tennis</a></li>*/
/* 			            <li><a href="shop.html">Tennis Shoes</a></li>*/
/* 			            <li><a href="shop.html">Tennis Clothing</a></li>*/
/* 			            <li><a href="shop.html">Training</a></li>*/
/* 			            <li><a href="shop.html">Training Shoes</a></li>*/
/* 			            <li><a href="shop.html">Training Clothing</a></li>*/
/* 			            <li><a href="shop.html">Training Accessories</a></li>*/
/* 			            <li><a href="shop.html">miCoach</a></li>*/
/* 			            <li><a href="shop.html">All Sports</a></li>*/
/* 			         </ul>*/
/* 				</div>*/
/* 				<div class="col_1_of_5 span_1_of_5">*/
/* 					<h3 class="m_9">Originals</h3>*/
/* 					<ul class="list1">*/
/* 					    <li><a href="shop.html">Originals Shoes</a></li>*/
/* 			            <li><a href="shop.html">Gazelle</a></li>*/
/* 			            <li><a href="shop.html">Samba</a></li>*/
/* 			            <li><a href="shop.html">LA Trainer</a></li>*/
/* 			            <li><a href="shop.html">Superstar</a></li>*/
/* 			            <li><a href="shop.html">SL</a></li>*/
/* 			            <li><a href="shop.html">ZX</a></li>*/
/* 			            <li><a href="shop.html">Campus</a></li>*/
/* 			            <li><a href="shop.html">Spezial</a></li>*/
/* 			            <li><a href="shop.html">Dragon</a></li>*/
/* 			            <li><a href="shop.html">Originals Clothing</a></li>*/
/* 			            <li><a href="shop.html">Firebird</a></li>*/
/* 			            <li><a href="shop.html">Originals Accessories</a></li>*/
/* 			            <li><a href="shop.html">Men's Originals</a></li>*/
/* 			            <li><a href="shop.html">Women's Originals</a></li>*/
/* 			            <li><a href="shop.html">Kid's Originals</a></li>*/
/* 			            <li><a href="shop.html">All Originals</a></li>*/
/* 		            </ul>*/
/* 				</div>*/
/* 				<div class="col_1_of_5 span_1_of_5">*/
/* 					<h3 class="m_9">Product Types</h3>*/
/* 					<ul class="list1">*/
/* 					    <li><a href="shop.html">Shirts</a></li>*/
/* 					    <li><a href="shop.html">Pants & Tights</a></li>*/
/* 					    <li><a href="shop.html">Shirts</a></li>*/
/* 					    <li><a href="shop.html">Jerseys</a></li>*/
/* 					    <li><a href="shop.html">Hoodies & Track Tops</a></li>*/
/* 					    <li><a href="shop.html">Bags</a></li>*/
/* 					    <li><a href="shop.html">Jackets</a></li>*/
/* 					    <li><a href="shop.html">Hi Tops</a></li>*/
/* 					    <li><a href="shop.html">SweatShirts</a></li>*/
/* 					    <li><a href="shop.html">Socks</a></li>*/
/* 					    <li><a href="shop.html">Swimwear</a></li>*/
/* 					    <li><a href="shop.html">Tracksuits</a></li>*/
/* 					    <li><a href="shop.html">Hats</a></li>*/
/* 					    <li><a href="shop.html">Football Boots</a></li>*/
/* 					    <li><a href="shop.html">Other Accessories</a></li>*/
/* 					    <li><a href="shop.html">Sandals & Flip Flops</a></li>*/
/* 					    <li><a href="shop.html">Skirts & Dresseses</a></li>*/
/* 					    <li><a href="shop.html">Balls</a></li>*/
/* 					    <li><a href="shop.html">Watches</a></li>*/
/* 					    <li><a href="shop.html">Fitness Equipment</a></li>*/
/* 					    <li><a href="shop.html">Eyewear</a></li>*/
/* 					    <li><a href="shop.html">Gloves</a></li>*/
/* 					    <li><a href="shop.html">Sports Bras</a></li>*/
/* 					    <li><a href="shop.html">Scarves</a></li>*/
/* 					    <li><a href="shop.html">Shinguards</a></li>*/
/* 					    <li><a href="shop.html">Underwear</a></li>*/
/* 		            </ul>*/
/* 				</div>*/
/* 				<div class="col_1_of_5 span_1_of_5">*/
/* 					<h3 class="m_9">Support</h3>*/
/* 					<ul class="list1">*/
/* 					   <li><a href="shop.html">Store finder</a></li>*/
/* 					   <li><a href="shop.html">Customer Service</a></li>*/
/* 					   <li><a href="shop.html">FAQ</a></li>*/
/* 					   <li><a href="shop.html">Online Shop Contact Us</a></li>*/
/* 					   <li><a href="shop.html">about adidas Products</a></li>*/
/* 					   <li><a href="shop.html">Size Charts </a></li>*/
/* 					   <li><a href="shop.html">Ordering </a></li>*/
/* 					   <li><a href="shop.html">Payment </a></li>*/
/* 					   <li><a href="shop.html">Shipping </a></li>*/
/* 					   <li><a href="shop.html">Returning</a></li>*/
/* 					   <li><a href="shop.html">Using out Site</a></li>*/
/* 					   <li><a href="shop.html">Delivery Terms</a></li>*/
/* 					   <li><a href="shop.html">Site Map</a></li>*/
/* 					   <li><a href="shop.html">Gift Card</a></li>*/
/* 					  */
/* 		            </ul>*/
/* 		            <ul class="sub_list2">*/
/* 		               <h4 class="m_10">Company Info</h4>*/
/* 			           <li><a href="{{asset('shop.html')}}">About Us</a></li>*/
/* 			           <li><a href="{{asset('shop.html')}}">Careers</a></li>*/
/* 			           <li><a href="{{asset('shop.html')}}">Press</a></li>*/
/* 			        </ul>*/
/* 				</div>*/
/* 				<div class="clear"></div>*/
/* 			</div>*/
/*        	 	</div>*/
/*        	 </div>*/
/*        	 <div class="copy">*/
/*        	   <div class="wrap">*/
/*        	   	  <p>� All rights reserved | Template by&nbsp;<a href="{{asset('http://w3layouts.com/')}}"> W3Layouts</a></p>*/
/*        	   </div>*/
/*        	 </div>*/
/*        </div>*/
/*        <script type="text/javascript">*/
/* 			$(document).ready(function() {*/
/* 			*/
/* 				var defaults = {*/
/* 		  			containerID: 'toTop', // fading element id*/
/* 					containerHoverID: 'toTopHover', // fading element hover id*/
/* 					scrollSpeed: 1200,*/
/* 					easingType: 'linear' */
/* 		 		};*/
/* 				*/
/* 				*/
/* 				$().UItoTop({ easingType: 'easeOutQuart' });*/
/* 				*/
/* 			});*/
/* 		</script>*/
/*         <a href="{{asset('#')}}" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>*/
/* </body>*/
/* </html>*/
/* */
