<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MyApp\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description of User
 *
 * @author MohamedChiheb
 * @author MohamedChiheb
 */
/**
 *  @ORM\Entity
 */
class User extends BaseUser {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * 
     * @ORM\Column(type="string", length=255)
     */
    protected $nom;
    
    /**
     * 
     * @ORM\Column(type="string", length=255)
     */
    protected $prenom;
    
    public function __construct()
            {
        parent::__construct();
        // your own logic
        
            }
    
            function getId() {
                return $this->id;
            }

            function getNom() {
                return $this->nom;
            }

            function getPrenom() {
                return $this->prenom;
            }

            function setId($id) {
                $this->id = $id;
            }

            function setNom($nom) {
                $this->nom = $nom;
            }

            function setPrenom($prenom) {
                $this->prenom = $prenom;
            }


}
